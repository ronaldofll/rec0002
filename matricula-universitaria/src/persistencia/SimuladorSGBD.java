package persistencia;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import entidades.Aluno;
import entidades.Curso;
import entidades.Disciplina;
import entidades.Entidade;
import entidades.Matricula;
import enumeration.EstadoMatricula;

public class SimuladorSGBD {
	
	Map<String, Map<String, Entidade>> banco;
	
	Map<String, Integer> ultimoId;
	
	public SimuladorSGBD() {
		banco = new LinkedHashMap<>();
		ultimoId = new LinkedHashMap<>();
	}
	
	public Entidade add(Entidade entidade) throws Exception {
		String nomeTabela = entidade.getClass().getSimpleName();
		
		entidade.validar(this);
		
		Map<String, Entidade> tabela = banco.get(nomeTabela);
		if(tabela == null) { 
			tabela = new HashMap<>();
			banco.put(nomeTabela, tabela);
			ultimoId.put(nomeTabela, 0);
		}

		Integer id = null;
		for(Entidade entidade1 : tabela.values()) {
			if(entidade.compareTo(entidade1)) {
				id = entidade1.getId();
				break;
			}
		}
		
		if(id == null) {
			id = ultimoId.get(nomeTabela) + 1;			
			ultimoId.put(nomeTabela, id);
		}

		entidade.setId(id);
		tabela.put(entidade.key(), entidade);
		
		return entidade;
	}

	public Entidade get(String nomeTabela, String chave) {
		Map<String, Entidade> tabela = banco.get(nomeTabela);
		
		Entidade entidade = null;
		if(tabela != null) {
			entidade = tabela.get(chave);
		}
		
		if(entidade == null) {
			System.out.println("Registro com chave " + chave + " da tabela " + nomeTabela + " não encontrado!");
		}
		
		return entidade;
	}
	
	public void excluir(Entidade entidade) {
		Map<String, Entidade> tabela = banco.get(entidade.getClass().getSimpleName());
		if(tabela != null && tabela.containsKey(entidade.key())) {
			tabela.remove(entidade.key());
		} else {
			System.out.println("Registro com chave " + entidade.key() + " da tabela " + entidade.getClass().getSimpleName() + " não encontrado!");
		}
	}
	
	public void atualizar(Entidade entidade) throws Exception {
		Entidade excluir = getById(entidade.getClass().getSimpleName(), entidade.getId());
		excluir(excluir);
		add(entidade);
	}

	public Entidade getById(String nomeTabela, Integer id) {
		Map<String, Entidade> tabela = banco.get(nomeTabela);
		
		Entidade entidade = null;
		if(tabela != null) {
			for(Entidade e : tabela.values()) {
				if(e.getId().equals(id)) {
					entidade = e;
				}
			}
		}
		
		return entidade;		
	}
	
	public boolean existe(String nomeTabela, String chave) {
		Map<String, Entidade> tabela = banco.get(nomeTabela);
		return tabela != null && tabela.containsKey(chave);
	}
	
	public Disciplina getDisciplinaByCode(String cod) {

		Map<String, Entidade> tabela = banco.get(Disciplina.class.getSimpleName());
		Disciplina disciplina = null;
		
		if(tabela != null) {
			for(Entidade entidade : tabela.values()) {
				Disciplina d = (Disciplina) entidade;
				if(d.getCod().equalsIgnoreCase(cod)) {
					disciplina = d;
				}
			}
		}
			
		if(disciplina == null) {
			System.out.println("Registro com cod " + cod + " da tabela " + Disciplina.class.getSimpleName() + " não encontrado!");
		}
		return disciplina;
	}
	
	public List<Matricula> listaDisciplinasPorAluno(Integer alunoId) {

		Map<String, Entidade> tabela = banco.get(Matricula.class.getSimpleName());
		List<Matricula> matriculas = new ArrayList<>();
		
		if(tabela != null) {
			for(Entidade entidade : tabela.values()) {
				Matricula m = (Matricula) entidade;
				if(m.getAlunoId().intValue() == alunoId && m.getEstadoMatricula().equals(EstadoMatricula.APROVADO)) {
					matriculas.add(m);
				}
			}
		}
		
		return matriculas;
	}
	
	public void mostrarTabelas() {
		Iterator<String> i = banco.keySet().iterator();
		while(i.hasNext()) {
			String nomeTabela = i.next();
			System.out.println("\n" + nomeTabela+"s");
			
			boolean first = true;
			
			for(Entidade entidade : banco.get(nomeTabela).values()) {
				if(first) {
					System.out.println(entidade.logger());
					first = false;
				}
				System.out.println(entidade.toString());
			}
		}
	}

	public List<Entidade> listaCursoParaAluno(Aluno aluno) {
		
		List<Entidade> cursos = new ArrayList<>();
		
		Map<String, Entidade> tabelaCursos = banco.get(Curso.class.getSimpleName());
		Map<String, Entidade> tabelaMatriculas = banco.get(Matricula.class.getSimpleName());
		
		if(tabelaCursos != null) {
			for(Entidade cursoE : tabelaCursos.values()) {
				Curso curso = (Curso) cursoE;
				if(tabelaMatriculas != null) {
					boolean jaMatriculado = false;
					for(Entidade matriculaE : tabelaMatriculas.values()) {
						Matricula m = (Matricula) matriculaE;
						if(m.getAlunoId().intValue() == aluno.getId() || !m.getCursoId().equals(curso.getId())) {
							jaMatriculado = true;
						}
					}
					
					if(!jaMatriculado) {
						cursos.add(curso);
					}
				} else {
					cursos.add(curso);					
				}
			}
		}
		
		return cursos;
	}

	public List<Entidade> listarDisciplinasRecomendadas(Integer cursoId, String cpfAluno) {
		
		List<Entidade> disciplinasRecomendadas = new ArrayList<>();
		
		Aluno aluno = (Aluno) get(Aluno.class.getSimpleName(), cpfAluno);
		
		Map<String, Entidade> tabelaMatriculas = banco.get(Matricula.class.getSimpleName());
		Map<String, Entidade> tabelaDisciplinas = banco.get(Disciplina.class.getSimpleName());	
		
		if(tabelaDisciplinas != null) {
			for(Entidade disciplinaE : tabelaDisciplinas.values()) {
				
				Disciplina disciplina = (Disciplina) disciplinaE;
				
				boolean temDependencia = false;
				
				if(disciplina.getDependencias() != null && !disciplina.getDependencias().isEmpty()) {
					for(String codDisciplina : disciplina.getDependencias()) {
						
						Disciplina dependencia = (Disciplina) tabelaDisciplinas.get(codDisciplina); 
						
						boolean aprovado = false;
						if(tabelaMatriculas != null) {
							for(Entidade matriculaE : tabelaMatriculas.values()) {
								Matricula matricula = (Matricula) matriculaE;
								if(aluno.getId().equals(matricula.getAlunoId()) 
										&& matricula.getCursoId().equals(cursoId)
										&& dependencia.getId().equals(matricula.getDisciplinaId()) 
										&& matricula.getEstadoMatricula().equals(EstadoMatricula.APROVADO)) {
									aprovado = true;
								}							
							}
						}
						
						if(!aprovado) {
							temDependencia = true;
							break;
						}
						
					}
				}
				
				if(!temDependencia) {
					
					boolean jaMatriculado = false;
					if(tabelaMatriculas != null) {
						for(Entidade matriculaE : tabelaMatriculas.values()) {
							Matricula matricula = (Matricula) matriculaE;
							if(aluno.getId().equals(matricula.getAlunoId()) && 
									matricula.getCursoId().equals(cursoId) &&
									disciplina.getId().equals(matricula.getDisciplinaId())) {
								jaMatriculado = true;
							}
						}
					}
					
					if(!jaMatriculado) {
						disciplinasRecomendadas.add(disciplina);
						
						if(disciplinasRecomendadas.size() == 5) {
							return disciplinasRecomendadas;
						}
					}
				}
			}
		}
		
		Collections.sort(disciplinasRecomendadas, new Comparator<Entidade>() {
			@Override
			public int compare(Entidade o1, Entidade o2) {
				Matricula a = (Matricula) o1;
				Matricula b = (Matricula) o2;
				return a.getId().compareTo(b.getId());
			}
		});
		
		return disciplinasRecomendadas;		
	}

	public List<Entidade> listarMatriculasPorAluno(Aluno aluno) {
		Map<String, Entidade> tabelaMatriculas = banco.get(Matricula.class.getSimpleName());
		
		List<Entidade> matriculas = new ArrayList<>();
		
		if(tabelaMatriculas != null) {
			for(Entidade entidade : tabelaMatriculas.values()) {
				Matricula matricula = (Matricula) entidade;
				if(matricula.getAlunoId().equals(aluno.getId())) {
					matriculas.add(matricula);
				}
			}
		}
		
		return matriculas;		
	}
}
