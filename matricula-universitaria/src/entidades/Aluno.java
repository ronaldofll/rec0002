package entidades;

import java.io.Serializable;

import persistencia.SimuladorSGBD;

public class Aluno extends Entidade implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String cpf;
	private String senha;
	
	public Aluno(String nome, String cpf, String senha) {
		super(); 
		this.nome = nome;
		this.cpf = cpf;
		this.senha = senha;
	}
	
	public Aluno(String cpf, String senha) {
		super();
		this.cpf = cpf;
		this.senha = senha;
	}

	@Override
	public String key() {
		return cpf;
	}

	@Override
	public void validar(SimuladorSGBD sgbd) throws Exception {
		if(sgbd.existe(this.getClass().getSimpleName(), key())) {
			throw new Exception("Aluno já está cadastrado!");
		}
	}

	@Override
	public boolean compareTo(Entidade entidade) {
		if(entidade instanceof Aluno) {
			if(entidade.getId() != null && this.getId() != null && entidade.getId().intValue() == this.getId().intValue()) {
				return true;
			} else {
				if(((Aluno)entidade).cpf.equalsIgnoreCase(this.cpf)) {
					return true;
				}
			}
		}
		
		return false;	
	}
	
	public String logger() {
		StringBuilder s = new StringBuilder();
		s.append(super.logger());
		s.append(Utils.leftPad("NOME", 30));
		s.append(Utils.leftPad("CPF", 20));
		return s.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(super.toString());
		s.append(Utils.leftPad(nome, 30));
		s.append(Utils.leftPad(cpf, 20));
		return s.toString();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
