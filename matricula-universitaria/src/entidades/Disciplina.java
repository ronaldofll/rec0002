package entidades;

import java.io.Serializable;
import java.util.List;

import persistencia.SimuladorSGBD;

public class Disciplina extends Entidade implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String cod;
	String nome;
	List<String> dependencias;

	@Override
	public String key() {
		return cod;
	}

	@Override
	public void validar(SimuladorSGBD sgbd) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean compareTo(Entidade entidade) {
		if(entidade instanceof Disciplina) {
			if(entidade.getId() != null && this.getId() != null && entidade.getId().intValue() == this.getId().intValue()) {
				return true;
			}
		}
		
		return false;	
	}

	public String logger() {
		StringBuilder s = new StringBuilder();
		s.append(super.logger());
		s.append(Utils.leftPad("COD", 9));
		s.append(Utils.leftPad("NOME", 32));
		s.append("DEPENDÊNCIAS");
		return s.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(super.toString());
		s.append(Utils.leftPad(String.valueOf(cod), 9));
		s.append(Utils.leftPad(String.valueOf(nome), 32));
		s.append(dependencias);
		return s.toString();
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<String> getDependencias() {
		return dependencias;
	}

	public void setDependencias(List<String> dependencias) {
		this.dependencias = dependencias;
	}
}
