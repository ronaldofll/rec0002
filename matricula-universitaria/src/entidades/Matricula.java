package entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import enumeration.EstadoMatricula;
import persistencia.SimuladorSGBD;

public class Matricula extends Entidade implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer alunoId;
	private String nomeAluno;
	private Integer cursoId;
	private String codCurso;
	private Integer disciplinaId;
	private String codDisciplina;
	private Integer ano;
	private Integer semestre;
	private EstadoMatricula estadoMatricula;
	private Date dataMatricula;

	public Matricula(Integer alunoId, String nomeAluno, Integer curso, String codCurso, Integer disciplina, String codDisciplina, Integer ano, Integer semestre,	Date dataMatricula) {
		super();
		this.alunoId = alunoId;
		this.nomeAluno = nomeAluno;
		this.cursoId = curso;
		this.codCurso = codCurso;
		this.disciplinaId = disciplina;
		this.codDisciplina = codDisciplina;
		this.ano = ano;
		this.semestre = semestre;
		this.dataMatricula = dataMatricula;
		this.estadoMatricula = EstadoMatricula.CURSANDO;
	}

	public Matricula() {
	}

	@Override
	public String key() {
		return alunoId + "_" + cursoId + "_" + disciplinaId + "_" + semestre;
	}

	@Override
	public void validar(SimuladorSGBD sgbd) throws Exception {
		
		Disciplina disciplina = (Disciplina) sgbd.getById(Disciplina.class.getSimpleName(), disciplinaId);
		
		List<Matricula> cursadosPeloAluno = sgbd.listaDisciplinasPorAluno(alunoId);
		
		//Verifica se há dependências ainda não cursadas
		if(disciplina.getDependencias() != null && !disciplina.getDependencias().isEmpty()) {
			for(String cod : disciplina.getDependencias()) {
				Disciplina dependencia = sgbd.getDisciplinaByCode(cod);
				
				boolean cursou = false;
				for(Matricula cursadoPeloAluno : cursadosPeloAluno) {
					if(dependencia.getId().intValue() == cursadoPeloAluno.disciplinaId.intValue()) {
						cursou = true;
						break;
					}
				}
				
				if(!cursou) {
					throw new Exception("Não é possível cadastrar esta matéria, ainda não cursou todas as dependências");
				}
				
			}
		}
		
	}

	@Override
	public boolean compareTo(Entidade entidade) {
		if(entidade instanceof Matricula) {
			if(entidade.getId() != null && this.getId() != null && entidade.getId().intValue() == this.getId().intValue()) {
				return true;
			}
		}
		
		return false;	
	}

	public String logger() {
		StringBuilder s = new StringBuilder();
		s.append(super.logger());
		s.append(Utils.leftPad("ALUNO", 12));
		s.append(Utils.leftPad("CURSO", 10));
		s.append(Utils.leftPad("ID_DISCIPLINA", 32));
		s.append(Utils.leftPad("ANO", 6));
		s.append(Utils.leftPad("SEMESTRE", 10));
		s.append(Utils.leftPad("DATA_MATRICULA", 16));
		return s.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(super.toString());
		s.append(Utils.leftPad((nomeAluno != null ? nomeAluno : "null"), 12));
		s.append(Utils.leftPad((codCurso != null ? codCurso : "null"), 10));
		s.append(Utils.leftPad((codDisciplina != null ? codDisciplina : "null"), 32));
		s.append(Utils.leftPad(String.valueOf(ano), 6));
		s.append(Utils.leftPad(String.valueOf(semestre), 10));
		s.append(Utils.leftPad(Utils.dateFormat(dataMatricula), 16));
		return s.toString();
	}

	public Integer getAlunoId() {
		return alunoId;
	}

	public String getNomeAluno() {
		return nomeAluno;
	}

	public void setNomeAluno(String nomeAluno) {
		this.nomeAluno = nomeAluno;
	}

	public Integer getCursoId() {
		return cursoId;
	}

	public void setCursoId(Integer cursoId) {
		this.cursoId = cursoId;
	}

	public String getCodCurso() {
		return codCurso;
	}

	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}

	public Integer getDisciplinaId() {
		return disciplinaId;
	}

	public void setDisciplinaId(Integer disciplinaId) {
		this.disciplinaId = disciplinaId;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getSemestre() {
		return semestre;
	}

	public void setSemestre(Integer semestre) {
		this.semestre = semestre;
	}

	public EstadoMatricula getEstadoMatricula() {
		return estadoMatricula;
	}

	public void setEstadoMatricula(EstadoMatricula estadoMatricula) {
		this.estadoMatricula = estadoMatricula;
	}

	public Date getDataMatricula() {
		return dataMatricula;
	}

	public void setDataMatricula(Date dataMatricula) {
		this.dataMatricula = dataMatricula;
	}

	public void setAlunoId(Integer alunoId) {
		this.alunoId = alunoId;
	}

}
