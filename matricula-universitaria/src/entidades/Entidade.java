package entidades;

import java.io.Serializable;

import persistencia.SimuladorSGBD;

public abstract class Entidade implements Serializable{	
	private static final long serialVersionUID = 1L;
	
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public abstract void validar(SimuladorSGBD sgdb) throws Exception;
	
	public abstract boolean compareTo(Entidade entidade);
	
	public abstract String key();
	
	public String logger() {
		StringBuilder s = new StringBuilder();
		s.append(Utils.leftPad("ID", 8, null));
		return s.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(Utils.leftPad(String.valueOf(id), 8, null));
		return s.toString();
	}

}
