package entidades;

import java.io.Serializable;
import java.util.List;

import persistencia.SimuladorSGBD;

public class Curso extends Entidade implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String cod;
	String nome;
	List<Disciplina> disciplinas;

	@Override
	public String key() {
		return cod;
	}

	@Override
	public void validar(SimuladorSGBD sgbd) {
		// TODO Auto-generated method stub		
	}

	@Override
	public boolean compareTo(Entidade entidade) {
		if(entidade instanceof Curso) {
			if(entidade.getId() != null && this.getId() != null && entidade.getId().intValue() == this.getId().intValue()) {
				return true;
			}
		}
		
		return false;	
	}

	public String logger() {
		StringBuilder s = new StringBuilder();
		s.append(super.logger());
		s.append(Utils.leftPad("COD", 30));
		s.append(Utils.leftPad("NOME", 30));
		return s.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(super.toString());
		s.append(Utils.leftPad(String.valueOf(cod), 30));
		s.append(Utils.leftPad(String.valueOf(nome), 30));
		return s.toString();
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}
}
