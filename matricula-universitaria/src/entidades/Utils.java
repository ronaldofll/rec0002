package entidades;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
	
	public static String leftPad(String str, int size) {
		return leftPad(str, size, null);
	}
	
	public static String leftPad(String str, int size, String c) {
		
		if(str == null) {
			str = "";
		}
		
		if(str.length() >= size) {
			return str;
		}
		
		if(c == null) {
			c = " ";
		}
		
		StringBuilder s = new StringBuilder();
		s.append(str);
		for(int i = str.length(); i < size; i++) {
			s.append(c);
		}
		
		return s.toString();
	}
	
	public static String rightPad(String str, int size, String c) {
		
		if(str == null) {
			str = "";
		}
		
		if(str.length() >= size) {
			return str;
		}
		
		if(c == null) {
			c = " ";
		}
		
		StringBuilder s = new StringBuilder();
		for(int i = str.length(); i < size; i++) {
			s.append(c);
		}
		s.append(str);
		
		return s.toString();
	}
	
	
	public static boolean isEmpty(String str) {
		return str == null || str.isEmpty();
	}
	
	public static String dateFormat(Date date) { 
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(date);
	}
	
	public static Date parseDate(String date) throws ParseException { 
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.parse(date);
	}

	public static Integer portaAleatoria() {
		return numeroAleatorio(1024, 49151);
	}
	
	private static Integer numeroAleatorio(int min, int max) {
		return min + (int)(Math.random() * ((max - min) + 1));
		
	}
	

}
