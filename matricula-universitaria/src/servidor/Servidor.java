package servidor;
import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import entidades.Curso;
import entidades.Disciplina;
import entidades.Utils;
import persistencia.SimuladorSGBD;

public class Servidor {
	
	private Integer porta;
	
	ServerSocket socket;
	
	SimuladorSGBD sgbd = new SimuladorSGBD();
	
	public static void main(String[] args) throws Exception {
				
		Servidor servidor = new Servidor();
		servidor.incluirCursos();
		servidor.inicializarServidor(8180);
		
		servidor.sgbd.mostrarTabelas();

		while(true) {
			Socket client = servidor.socket.accept();
			
			RecebedorMensagemCliente recebedor = new RecebedorMensagemCliente(servidor.sgbd, client);
			new Thread(recebedor).start();
		}

	}
	
	public void inicializarServidor(Integer porta) throws IOException {
		try {
			if(porta != null) {
				this.porta = porta;
			} else {
				this.porta = Utils.portaAleatoria();
			}
			System.out.println("\n\nIniciando servidor na porta " + this.porta);
			socket = new ServerSocket(this.porta);
			System.out.println("Servidor iniciado!");
		} catch (BindException e) {
			System.out.println("Porta " + porta + " já está ocupada!");
			inicializarServidor(null);
		} catch (IOException e1) {
			System.out.println("Não foi possível inicializar servidor!");
			throw e1;
		}	
	}
	
	private void incluirCursos() throws Exception {
		//[{"cod":"TADS","nome":"Tecnologia em análise e desenvolvimento de sistemas","disciplinas":[{"cod":"AGT0001","nome":"Algoritmos","dependencias":[]},{"cod":"ALGA001","nome":"Geom. Analítica e Álg. Linear","dependencias":[]},{"cod":"CDI0001","nome":"Cálculo Diferencial e Integral","dependencias":[]},{"cod":"MCI0001","nome":"Metodologia Científica","dependencias":[]},{"cod":"TGA0002","nome":"Teoria Geral da Administração","dependencias":[]},{"cod":"AOC0002","nome":"Arquit. e Org. de Computadores","dependencias":["AGT0001"]},{"cod":"EST0006","nome":"Probabilidade e Estatística","dependencias":[]},{"cod":"LPG0002","nome":"Linguagem de Programação","dependencias":["AGT0001"]},{"cod":"MAT0002","nome":"Matemática Financeira","dependencias":[]},{"cod":"TGS0002","nome":"Teoria Geral de Sistemas","dependencias":[]}]}]
		String json = "[{\"cod\":\"TADS\",\"nome\":\"Tecnologia em análise e desenvolvimento de sistemas\",\"disciplinas\":[{\"cod\":\"AGT0001\",\"nome\":\"Algoritmos\",\"dependencias\":[]},{\"cod\":\"ALGA001\",\"nome\":\"Geom. Analítica e Álg. Linear\",\"dependencias\":[]},{\"cod\":\"CDI0001\",\"nome\":\"Cálculo Diferencial e Integral\",\"dependencias\":[]},{\"cod\":\"MCI0001\",\"nome\":\"Metodologia Científica\",\"dependencias\":[]},{\"cod\":\"TGA0002\",\"nome\":\"Teoria Geral da Administração\",\"dependencias\":[]},{\"cod\":\"AOC0002\",\"nome\":\"Arquit. e Org. de Computadores\",\"dependencias\":[\"AGT0001\"]},{\"cod\":\"EST0006\",\"nome\":\"Probabilidade e Estatística\",\"dependencias\":[]},{\"cod\":\"LPG0002\",\"nome\":\"Linguagem de Programação\",\"dependencias\":[\"AGT0001\"]},{\"cod\":\"MAT0002\",\"nome\":\"Matemática Financeira\",\"dependencias\":[]},{\"cod\":\"TGS0002\",\"nome\":\"Teoria Geral de Sistemas\",\"dependencias\":[]}]}]";
		
		ObjectMapper mapper = new ObjectMapper();
		Curso[] cursos = mapper.readValue(json, Curso[].class);
		
		List<Curso> cursoList = (cursos != null ? (List<Curso>) Arrays.asList(cursos) : new ArrayList<Curso>());
		
		for(Curso curso : cursoList) {
			for(Disciplina disciplina : curso.getDisciplinas()) {
				sgbd.add(disciplina);
			}
			
			sgbd.add(curso);
		}		
	}
}
