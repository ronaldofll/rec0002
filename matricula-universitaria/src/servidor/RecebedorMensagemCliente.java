package servidor;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

import conexao.Mensagem;
import conexao.ParametroListarDisciplinasRecomendadas;
import entidades.Aluno;
import entidades.Entidade;
import entidades.Matricula;
import enumeration.EstadoMatricula;
import persistencia.SimuladorSGBD;

public class RecebedorMensagemCliente implements Runnable {
		private Socket cliente;
		
		SimuladorSGBD sgbd;

		public RecebedorMensagemCliente(SimuladorSGBD simuladorSGBD, Socket cliente) {
			this.sgbd = simuladorSGBD;
			this.cliente = cliente;
		}

		public void run() {
			try {
				ObjectInputStream ois = new ObjectInputStream(cliente.getInputStream());
				ObjectOutputStream oos = new ObjectOutputStream(cliente.getOutputStream());
				Object obj = ois.readObject();
				
				Mensagem retorno = null;
				if(obj instanceof Mensagem) {
					retorno = tratar((Mensagem)obj);	
					//sgbd.mostrarTabelas();
				} else {
					retorno = new Mensagem();
					retorno.setProblema("O sistema não aceita este tipo de dados");
				}
								
				try{
					oos.writeObject(retorno);
					cliente.getOutputStream().flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		private Mensagem tratar(Mensagem mensagem) {			
			
			Mensagem retorno = new Mensagem();
			Entidade entidadeRetorno = null;
			List<Entidade> listaEntidadeRetorno = null;
			try {

				switch (mensagem.getTipo()) {
				case CADASTRO_ALUNO:
					entidadeRetorno = cadastraAluno((Aluno)mensagem.getEntidade());
					break;
				case LOGIN_ALUNO:
					entidadeRetorno = loginAluno((Aluno)mensagem.getEntidade());
					break;
				case CADASTRO_MATRICULA:
					entidadeRetorno = cadastraMatricula((Matricula)mensagem.getEntidade());
					break;
				case LISTAR_CURSOS:
					listaEntidadeRetorno = listarCursos((Aluno)mensagem.getEntidade());
					break;
				case LISTAR_DISCIPLINAS_RECOMENDADAS:
					ParametroListarDisciplinasRecomendadas parametro = (ParametroListarDisciplinasRecomendadas) mensagem.getEntidade();
					listaEntidadeRetorno = listarDisciplinasRecomendadas(parametro.getCursoId(), parametro.getCpfAluno());
					break;
				case LISTAR_MATRICULAS:
					listaEntidadeRetorno = listarMatriculas((Aluno) mensagem.getEntidade());
					break;
				case EXCLUIR_MATRICULA:
					excluirMatricula((Matricula) mensagem.getEntidade());
					break;
				case EDITAR_MATRICULA:
					editarMatricula((Matricula) mensagem.getEntidade());
					break;
				case CONCLUIR_DISCIPLINA:
					Matricula matricula = (Matricula) mensagem.getEntidade();
					matricula.setEstadoMatricula(EstadoMatricula.APROVADO);
					editarMatricula(matricula);
					break;
				default:
					break;
				}
			
			} catch (Exception e) {
				e.printStackTrace();
				retorno.setProblema(e.getMessage());
			}
			
			retorno.setEntidade(entidadeRetorno);
			retorno.setLista(listaEntidadeRetorno);
			
			System.out.println("\n\n\n");
			sgbd.mostrarTabelas();
			return retorno;
		}

		private Entidade cadastraAluno(Aluno aluno) throws Exception {			
			return sgbd.add(aluno);
		}
		
		private Entidade loginAluno(Aluno aluno) throws Exception {
			
			Aluno logado = (Aluno) sgbd.get(Aluno.class.getSimpleName(), aluno.key());
			
			//valida cpf e senha
			if(logado == null) {
				throw new Exception("CPF inválido");
			} else if(aluno.getSenha() == null || !logado.getSenha().equals(aluno.getSenha())) {
				throw new Exception("Senha inválida");
			}
			
			return logado;					
		}
		
		private Entidade cadastraMatricula(Matricula matricula) throws Exception {
			return sgbd.add(matricula);
		}
		
		private List<Entidade> listarCursos(Aluno aluno) {
			return sgbd.listaCursoParaAluno(aluno);
		}
		
		private List<Entidade> listarDisciplinasRecomendadas(Integer cursoId, String cpfAluno) {
			return sgbd.listarDisciplinasRecomendadas(cursoId, cpfAluno);
		}
		
		private List<Entidade> listarMatriculas(Aluno aluno) {
			return sgbd.listarMatriculasPorAluno(aluno);
		}

		private void excluirMatricula(Matricula matricula) {
			sgbd.excluir(matricula);
		}

		private void editarMatricula(Matricula matricula) throws Exception {
			sgbd.atualizar(matricula);
		}
	}
