package enumeration;

public enum EstadoMatricula {
	CURSANDO,
	APROVADO,
	REPROVADO
}
