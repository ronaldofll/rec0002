package cliente;



import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import conexao.Conexao;
import conexao.Mensagem;
import conexao.ParametroListarDisciplinasRecomendadas;
import conexao.TipoMensagem;
import entidades.Aluno;
import entidades.Curso;
import entidades.Disciplina;
import entidades.Entidade;
import entidades.Matricula;

// 2. Controle de Matrícula Universitária
// Implementar uma aplicação cliente-servidor que permita ao aluno incluir, excluir
// e alterar sua matrícula em disciplinas de cursos de uma universidade. A aplicação
// também deve verificar os pré-requisitos necessários entre as disciplinas e apresentar
// uma sugestão de matrícula para o aluno.

public class Cliente {
	
	private static Aluno alunoLogado;
	
	private static Scanner s;
	
	static String ip = "127.0.0.1";
	static int porta = 8180;
	
	public static void main (String[] args) throws IOException {
		s = new Scanner(System.in);
		
		while(new Conexao(ip, porta).socket == null) {			
			System.out.print("Digite o endereço ip do servidor: ");
			ip = s.nextLine();
			System.out.print("Digite a porta de conexão: ");
			
			try {
				porta = Integer.parseInt(s.nextLine());
			} catch(Exception e) {
				porta = 0;
			}
		};
		
		menuPrincipal();
		s.close();
	}
	
	private static void menuPrincipal() {

		String opc = "";
		while(!opc.equals("3")) {
		
			System.out.println("\n1 - Login");
			System.out.println("2 - Cadastro");
			System.out.println("3 - Sair");
			System.out.print("Escolha uma das opções: ");
			
	
			opc = s.nextLine();
			
			switch (opc) {
			case "1":
				menuLogin();
				break;
				
			case "2": 
				menuCadastro();
				break;
				
			case "3": 
				System.out.println("\nSaindo...");
				break;		
				
			default:
				System.err.println("\nOpção inválida!");
				break;	
				
			}
		}
	}
	
	private static void menuLogin() {
		
		System.out.print("\nDigite seu CPF: ");
		String cpf = s.nextLine();
		
		System.out.print("\nDigite uma senha: ");
		String senha = s.nextLine();
		
		alunoLogado = logarAluno(cpf, senha);
		if(alunoLogado != null) {
			menuLogado();
		}
	}
	
	private static void menuCadastro()  {
		
		System.out.print("\nDigite seu nome: ");
		String nome = s.nextLine();
		
		System.out.print("\nDigite seu CPF: ");
		String cpf = s.nextLine();
		
		System.out.print("\nDigite uma senha: ");
		String senha = s.nextLine();
		
		Aluno aluno = new Aluno(nome, cpf, senha);
		
		alunoLogado = cadastrarAluno(aluno);
		if(alunoLogado != null) {
			menuLogado();
		}
	}
	
	private static Aluno cadastrarAluno(Aluno aluno) { 
		System.out.println("\nCadastrando aluno: ");
		System.out.println(aluno.logger());
		System.out.println(aluno);
		
		Mensagem mensagem = new Mensagem(TipoMensagem.CADASTRO_ALUNO, aluno);
	
		Conexao conexao = new Conexao(ip, porta);
		Mensagem retorno = conexao.enviar(mensagem);
		
		if(retorno.getProblema() != null) {
			System.err.println("\n" + retorno.getProblema());
			aluno = null;
		} else {
			System.out.println("\nAluno(a) cadastrada com sucesso!");
			aluno = (Aluno) retorno.getEntidade();
		}
		
		return aluno;
	}
	
	private static void menuLogado() {

		String opc = "";
		while(!opc.equals("5")) {
			System.out.println("\n1 - Incluir matrícula");
			System.out.println("2 - Excluir matrícula");
			System.out.println("3 - Alterar matrícula");
			System.out.println("4 - Simulação(Aprovar na disciplina)");
			System.out.println("5 - Sair");
			System.out.print("Escolha uma das opções: ");

			opc = s.nextLine();
		
			switch (opc) {
			case "1":
				menuIncluirMatricula();
				break;			
			case "2":
				menuExcluirMatricula();
				break;			
			case "3":
				menuAlterarMatricula();
				break;			
			case "4":
				menuConcluirDisciplina();
				break;			
			case "5":
				break;
			default:
				System.err.println("\nOpção inválida!");
				menuLogado();
				break;			
			}
		}
	}
	
	private static void menuConcluirDisciplina() {
		List<Matricula> listaMatricula = listarMatriculas(alunoLogado);
		
		if(listaMatricula == null || listaMatricula.isEmpty()) {
			System.out.println("\nNenhuma matrícula cadastrada");
		} else {
		
			System.out.println(new Matricula().logger());
			for(Matricula matricula : listaMatricula) {
				System.out.println(matricula);
			}
			
			System.out.print("\nDigite o ID: ");
	
			Matricula matricula = null;
			while(matricula == null) {
				String cod = s.nextLine();
				
				for(Matricula m : listaMatricula) {
					if(String.valueOf(m.getId()).equals(cod)) {
						matricula = m;
						break;
					}
				}
				
				if(matricula == null) {
					System.out.print("\nMatrícula inválida, digite novamente: ");
				}
			}
			
			concluirDisciplina(matricula);
			
		}		
	}
	
	private static void concluirDisciplina(Matricula matricula) {
		Mensagem mensagem = new Mensagem(TipoMensagem.CONCLUIR_DISCIPLINA, matricula);
		
		Conexao conexao = new Conexao(ip, porta);
		Mensagem retorno = conexao.enviar(mensagem);
		
		if(retorno.getProblema() == null) {
			System.out.println("\nDisciplina concluída.");
		} else {
			System.err.println("\nNão foi possível concluir a disciplina: " + retorno.getProblema());
		}		
	}

	private static void menuIncluirMatricula() {
		
		List<Curso> listaCursos = listarCursos(alunoLogado);
		
		if(listaCursos == null || listaCursos.isEmpty()) {
			System.out.println("\nNão há cursos disponíveis");
		} else {
		
			System.out.println(new Curso().logger());
			for(Curso curso : listaCursos) {
				System.out.println(curso);
			}
			
			System.out.print("\nDigite o código de um dos cursos acima: ");
	
			Curso curso = null;
			while(curso == null) {
				String cod = s.nextLine();
				
				for(Curso c : listaCursos) {
					if(c.getCod().equals(cod)) {
						curso = c;
						break;
					}
				}
				
				if(curso == null) {
					System.out.print("\nCurso inválido, digite novamente: ");
				}
			}
			
			List<Disciplina> listaDisciplinas = listarDisciplinasRecomendadas(curso.getId(), alunoLogado.getCpf());
	
			int quantidade = listaDisciplinas.size() >= 5 ? 5 : listaDisciplinas.size();
	
			if(quantidade > 0) {
				
				System.out.println("\nDisciplinas disponíveis: ");
				System.out.println(new Disciplina().logger());
				for(Disciplina disciplina : listaDisciplinas) {
					System.out.println(disciplina);
				}
				
				Integer ano = 2018; //TODO
				Integer semestre = 1; //TODO			
			
				for(int i = 0; i < quantidade; i++) {
					System.out.print("\nDigite o código da disciplina "+(i+1)+" (ou aperte 0 para concluir): ");
					
					Disciplina disciplina = null;
					while(disciplina == null) {
						String codDisciplina = s.nextLine();
						
						if(codDisciplina.equals("0")) {
							break;
						}
						
						for(Disciplina d : listaDisciplinas) {
							if(d.getCod().equals(codDisciplina)) {
								disciplina = d;
							}
						}
						
						if(disciplina == null) {
							System.out.print("\nDisciplina inválido, digite novamente: ");
						}
					}
					
					if(disciplina == null) {
						break;
					}
					
					Matricula matricula = new Matricula(alunoLogado.getId(), alunoLogado.getNome(), curso.getId(), curso.getCod(), disciplina.getId(), disciplina.getCod(), ano, semestre, new Date());
					cadastrarMatrícula(matricula);
				}
			} else {
				System.out.println("\nNão há disciplinas disponíveis.");
			}
		}
		
	}

	private static void menuAlterarMatricula() {
		
		List<Matricula> listaMatricula = listarMatriculas(alunoLogado);
		
		if(listaMatricula == null || listaMatricula.isEmpty()) {
			System.out.println("\nNenhuma matrícula cadastrada");
		} else {
		
			System.out.println(new Matricula().logger());
			for(Matricula matricula : listaMatricula) {
				System.out.println(matricula);
			}
			
			System.out.print("\nDigite o ID da matrícula: ");
	
			Matricula matricula = null;
			while(matricula == null) {
				String cod = s.nextLine();
				
				for(Matricula m : listaMatricula) {
					if(String.valueOf(m.getId()).equals(cod)) {
						matricula = m;
						break;
					}
				}
				
				if(matricula == null) {
					System.out.print("\nMatrícula inválida, digite novamente: ");
				}
			}
			
			List<Disciplina> listaDisciplinas = listarDisciplinasRecomendadas(matricula.getCursoId(), alunoLogado.getCpf());
			
			System.out.println("\nDisciplinas disponíveis: ");
			System.out.println(new Disciplina().logger());
			for(Disciplina disciplina : listaDisciplinas) {
				System.out.println(disciplina);
			}
			
			System.out.print("\nDigite o código da disciplina:");
			
			Disciplina disciplina = null;
			while(disciplina == null) {
				String codDisciplina = s.nextLine();
				
				if(codDisciplina.equals("0")) {
					break;
				}
				
				for(Disciplina d : listaDisciplinas) {
					if(d.getCod().equals(codDisciplina)) {
						disciplina = d;
					}
				}
				
				if(disciplina == null) {
					System.out.print("\nDisciplina inválido, digite novamente: ");
				}
			}
			
			Integer ano = 2018; //TODO
			Integer semestre = 1; //TODO
			
			Matricula matricula1 = new Matricula(alunoLogado.getId(), alunoLogado.getNome(), matricula.getCursoId(), matricula.getCodCurso(), disciplina.getId(), disciplina.getCod(), ano, semestre, new Date());
			matricula1.setId(matricula.getId());
			editarMatricula(matricula1);
		}		
	}
	
	private static void editarMatricula(Matricula matricula) {
		Mensagem mensagem = new Mensagem(TipoMensagem.EDITAR_MATRICULA, matricula);
		
		Conexao conexao = new Conexao(ip, porta);
		Mensagem retorno = conexao.enviar(mensagem);
		
		if(retorno.getProblema() == null) {
			System.out.println("\nMatrícula alterada.");
		} else {
			System.err.println("\nNão foi possível alterar a matrícula: " + retorno.getProblema());
		}
		
	}
	
	private static void menuExcluirMatricula() {
		
		List<Matricula> listaMatricula = listarMatriculas(alunoLogado);
		
		if(listaMatricula == null || listaMatricula.isEmpty()) {
			System.out.println("\nNenhuma matrícula cadastrada");
		} else {
		
			System.out.println(new Matricula().logger());
			for(Matricula matricula : listaMatricula) {
				System.out.println(matricula);
			}
			
			System.out.print("\nDigite o ID da matrícula: ");
	
			Matricula matricula = null;
			while(matricula == null) {
				String cod = s.nextLine();
				
				for(Matricula m : listaMatricula) {
					if(String.valueOf(m.getId()).equals(cod)) {
						matricula = m;
						break;
					}
				}
				
				if(matricula == null) {
					System.out.print("\nMatrícula inválida, digite novamente: ");
				}
			}
			excluirMatricula(matricula);
		}		
	}
	
	private static List<Matricula> listarMatriculas(Aluno aluno) {
		Mensagem mensagem = new Mensagem(TipoMensagem.LISTAR_MATRICULAS, aluno);
		
		Conexao conexao = new Conexao(ip, porta);
		Mensagem retorno = conexao.enviar(mensagem);
		
		List<Matricula> listaMatricula = new ArrayList<>();
		for(Entidade entidade : retorno.getLista()) {
			listaMatricula.add((Matricula) entidade);
		}
		
		return listaMatricula;
		
	}
	
	private static void excluirMatricula(Matricula matricula) {
		Mensagem mensagem = new Mensagem(TipoMensagem.EXCLUIR_MATRICULA, matricula);
		
		Conexao conexao = new Conexao(ip, porta);
		Mensagem retorno = conexao.enviar(mensagem);
		
		if(retorno.getProblema() == null) {
			System.out.println("\nMatrícula excluída.");
		} else {
			System.err.println("\nNão foi possível excluir a matricula: " + retorno.getProblema());
		}
	}
	
	private static List<Curso> listarCursos(Aluno aluno) {
		Mensagem mensagem = new Mensagem(TipoMensagem.LISTAR_CURSOS, aluno);
		
		Conexao conexao = new Conexao(ip, porta);
		Mensagem retorno = conexao.enviar(mensagem);
		
		List<Curso> listaCursos = new ArrayList<>();
		for(Entidade entidade : retorno.getLista()) {
			listaCursos.add((Curso) entidade);
		}
		
		return listaCursos;
	}
	
	private static List<Disciplina> listarDisciplinasRecomendadas(Integer cursoId, String cpf) {
		
		ParametroListarDisciplinasRecomendadas parametro = new ParametroListarDisciplinasRecomendadas();
		parametro.setCursoId(cursoId);
		parametro.setCpfAluno(cpf);
		
		Mensagem mensagem = new Mensagem(TipoMensagem.LISTAR_DISCIPLINAS_RECOMENDADAS, parametro);
		
		Conexao conexao = new Conexao(ip, porta);
		Mensagem retorno = conexao.enviar(mensagem);

		List<Disciplina> listaDisciplinasRecomendadas = new ArrayList<>();
		if(retorno.getProblema() == null) {
			if(retorno.getLista() != null) {
				for(Entidade entidade : retorno.getLista()) {
					listaDisciplinasRecomendadas.add((Disciplina) entidade);
				}
			}
		} else {
			System.err.println("\nNão foi possível listar as disciplinas: " + retorno.getProblema());
		}
		
		return listaDisciplinasRecomendadas;
	}
	
	private static Aluno logarAluno(String cpf, String senha) {
		
		Aluno aluno = new Aluno(cpf, senha);		
		
		Mensagem mensagem = new Mensagem(TipoMensagem.LOGIN_ALUNO, aluno);

		Conexao conexao = new Conexao(ip, porta);
		Mensagem retorno = conexao.enviar(mensagem);
		
		if(retorno.getProblema() == null) {
			aluno = (Aluno) retorno.getEntidade();
		} else {
			System.err.println("\nNão foi possível efetuar o login: " + retorno.getProblema());
			aluno = null;
		}
		
		return aluno;
	}
	
	private static void cadastrarMatrícula(Matricula matricula) {

		Conexao conexao = new Conexao(ip, porta);		
		
		Mensagem mensagem = new Mensagem(TipoMensagem.CADASTRO_MATRICULA, matricula);
		
		Mensagem retorno = conexao.enviar(mensagem);
		if(retorno.getProblema() == null) {
			System.out.println("\nMatricula concluída com sucesso");
		} else {
			System.out.println("\nNão foi possível efetuar a matrícula: " + retorno.getProblema());
		}
	}
}
