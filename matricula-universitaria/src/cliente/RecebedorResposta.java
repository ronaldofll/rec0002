package cliente;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import conexao.Mensagem;

public class RecebedorResposta implements Runnable {	
	
	private Socket socket;
	
	private Mensagem retorno;
		
	public RecebedorResposta(Socket socket) {
		this.socket = socket;
	}	

	@Override
	public void run() {	

		try(ObjectInputStream ois = new ObjectInputStream(socket.getInputStream())) {
			Object obj = null;
			obj = ois.readObject();
			
			if(obj instanceof Mensagem) {
				this.retorno = (Mensagem) obj;
			} else {
				this.retorno = new Mensagem("Ocorreu um problema ao receber dados do servidor!");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Mensagem getRetorno() {
		return retorno;
	}
}