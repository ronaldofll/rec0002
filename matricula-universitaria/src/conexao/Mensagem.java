package conexao;

import java.io.Serializable;
import java.util.List;

import entidades.Entidade;

public class Mensagem implements Serializable{
	private static final long serialVersionUID = 1L;
	
	TipoMensagem tipo;
	Entidade entidade;
	List<Entidade> lista;
	String problema;
	
	public Mensagem() {
		
	}
	
	public Mensagem(TipoMensagem tipo) {
		this.tipo = tipo;
	}	
	
	public Mensagem(TipoMensagem tipo, Entidade entidade) {
		this.tipo = tipo;
		this.entidade = entidade;
	}
	
	public Mensagem(String problema) {
		this.problema = problema;
	}	
	
	public TipoMensagem getTipo() {
		return tipo;
	}
	
	public void setTipo(TipoMensagem tipo) {
		this.tipo = tipo;
	}
	public Entidade getEntidade() {
		return entidade;
	}
	public void setEntidade(Entidade entidade) {
		this.entidade = entidade;
	}
	public List<Entidade> getLista() {
		return lista;
	}

	public void setLista(List<Entidade> lista) {
		this.lista = lista;
	}

	public String getProblema() {
		return problema;
	}

	public void setProblema(String problema) {
		this.problema = problema;
	}
}
