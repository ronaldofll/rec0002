package conexao;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Conexao {
	
	public Socket socket;
	//private RecebedorResposta recebedorResposta;
	
	//private Thread thread;
	
	public Conexao(String ip, int porta) {
		
		try {
			//socket = new Socket("127.0.0.1", 8180);
			socket = new Socket(ip, porta);
			
			//recebedorResposta = new RecebedorResposta(socket);
			
			//Cria thread que irá aguardar pelo retorno;
			//thread = new Thread(recebedorResposta);
			//thread.start();
					
		} catch (IOException e) {
			if(!ip.equals("127.0.0.1")) {
				System.out.println("\nNão foi possível estabelecer conexão!");
			}
		}
	}

	public Mensagem enviar(final Mensagem mensagem) {
		
		Mensagem retorno = null;
		
		try {
			System.out.println("\nAguarde...");
			
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(mensagem);
			
			retorno = aguardarResposta();
		
			//retorno = recebedorResposta.getRetorno();
			
		} catch (Exception e) {
			retorno = new Mensagem("Ocorreu um problema ao enviar dados ao servidor");
		}
		
		return retorno;
		
	}
	
	public Mensagem aguardarResposta() {		
		Mensagem retorno = null;
		
		try(ObjectInputStream ois = new ObjectInputStream(socket.getInputStream())) {
			Object obj = null;
			obj = ois.readObject();
			
			if(obj instanceof Mensagem) {
				retorno = (Mensagem) obj;
			} else {
				retorno = new Mensagem("Ocorreu um problema ao receber dados do servidor!");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return retorno;
	}
}
