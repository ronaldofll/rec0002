package conexao;

import entidades.Entidade;
import persistencia.SimuladorSGBD;

public class ParametroListarDisciplinasRecomendadas extends Entidade {
	private static final long serialVersionUID = 1L;
	
	Integer cursoId;
	String cpfAluno;
	
	public Integer getCursoId() {
		return cursoId;
	}
	public void setCursoId(Integer cursoId) {
		this.cursoId = cursoId;
	}
	public String getCpfAluno() {
		return cpfAluno;
	}
	public void setCpfAluno(String cpfAluno) {
		this.cpfAluno = cpfAluno;
	}
	
	@Override
	public void validar(SimuladorSGBD sgdb) throws Exception {
		
	}
	
	@Override
	public boolean compareTo(Entidade entidade) {
		return false;
	}
	
	@Override
	public String key() {
		return null;
	}
}
